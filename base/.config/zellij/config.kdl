default_layout "compact"
default_shell "zsh" // workaround to get filepicker to open shells for folders - see https://github.com/zellij-org/zellij/issues/3272
session_serialization false

keybinds {
    tab {
        bind "t" {
            NewTab {
                name "top"
                layout "top"
            }
            SwitchToMode "Normal"
        }
    }

    shared_except "locked" {
        bind "Ctrl b" {
            LaunchOrFocusPlugin "room" {
                floating true
            }
        }
        bind "F12" {
            Run "lazygit" {
                close_on_exit true
                floating true

                height "85%"
                width "90%"
                x "5%"
                y "10%"
            }
        }
    }
}

plugins {
    room location="https://github.com/rvcas/room/releases/download/v1.0.0/room.wasm" {
        ignore_case true
    }
    // zjstatus location="https://github.com/dj95/zjstatus/releases/download/v0.14.0/zjstatus.wasm" {
    zjstatus location="file:~/.config/zellij/plugins/zjstatus.wasm" {
        format_left  "#[fg=#FFFFFF,bold] {session} {mode} {tabs}"
        format_right "{notifications} {swap_layout}"

        mode_locked "#[fg=#FF00D9,bold] {name} "
        mode_normal "#[fg=#AFFF00,bold] {name} "
        mode_resize "#[fg=#D75F00,bold] {name} "
        mode_default_to_mode "resize"

        notification_format_unread           "#[bg=#000000,fg=#181825]#[bg=#181825,fg=#89B4FA,blink]  #[bg=#181825,fg=#89B4FA] {message} #[bg=#181825,fg=#000000]"
        notification_format_no_notifications ""
        notification_show_interval           "10"

        swap_layout_format        "#[bg=#8A8A8A,fg=#000000] #[bg=#8A8A8A,fg=#000000,bold]{name} #[bg=#000000,fg=#8A8A8A]"
        swap_layout_hide_if_empty "true"

        tab_normal "#[bg=#8A8A8A,fg=#000000] #[bg=#8A8A8A,fg=#000000,bold]{name} {sync_indicator}{fullscreen_indicator}{floating_indicator} #[bg=#000000,fg=#8A8A8A]"
        tab_active "#[bg=#AFFF00,fg=#000000] #[bg=#AFFF00,fg=#000000,bold]{name} {sync_indicator}{fullscreen_indicator}{floating_indicator} #[bg=#000000,fg=#AFFF00]"

        tab_sync_indicator       " "
        tab_fullscreen_indicator "□ "
        tab_floating_indicator   "󰉈 "
    }
}

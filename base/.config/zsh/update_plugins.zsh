antidote update

echo "Generate antidote plugins file"

rm -rf "$(antidote home)" "${ZDOTDIR}/plugins.sh"

for file in ${ZDOTDIR}/plugins/*; do
  antidote bundle < ${file} >> "${ZDOTDIR}/plugins.sh"
done

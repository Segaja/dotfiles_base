# XDG
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"


# curl
export CURL_HOME="${XDG_CONFIG_HOME}/curl"

# docker
export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1
export DOCKER_CONFIG="${XDG_CONFIG_HOME}/docker"

# editor
export EDITOR=vim

# go
export GOPATH="${XDG_DATA_HOME}/go",
export GOMODCACHE="${XDG_CACHE_HOME}/go/mod"

# k9s
export KUBE_EDITOR=k9s-editor

# mplayer
export MPLAYER_HOME="${XDG_CONFIG_HOME}/mplayer"
# nodejs
export NPM_CONFIG_CACHE="${XDG_CACHE_HOME}/npm"

# path
export PATH="${HOME}/.local/bin:/opt/insomnia:${PATH}"

# rust
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export RUSTUP_HOME="${XDG_DATA_HOME}/rustup"

# ssh
[ -z "${SSH_AUTH_SOCK}" ] && export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"

# StreamDeck
export STREAMDECK_UI_CONFIG="${XDG_STATE_HOME}/streamdeck_ui.json"

# X11
export XAUTHORITY="${XDG_RUNTIME_DIR}/Xauthority"
export XINITRC="${XDG_CONFIG_HOME}/X11/xinitrc"


# update dotfiles repositories
dotfilesDir="${HOME}/dotfiles"

for repo in "${dotfilesDir}"/*; do
  if [ -d "${repo}" ]; then
    git -C "${repo}" pull --rebase
  fi
done

type fastfetch > /dev/null && fastfetch

# autostart X11 on tty1
if [[ -z "${DISPLAY}" ]] && [[ "$(tty)" = /dev/tty1 ]]; then
    exec startx >> /dev/null 2>&1
fi

# powerlevel10k instant prompt
if [[ -r "${XDG_CACHE_HOME}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Setopts
unsetopt beep          # avoid "beep"ing
unsetopt case_glob     # Use Case-Insensitve Globbing.
unsetopt globdots      # * shouldn't match dotfiles. ever.
unsetopt hup           # Don't send SIGHUP to background processes when the shell exits.
unsetopt nomatch       # try to avoid the 'zsh: no matches found...'
unsetopt sh_word_split # use zsh style word splitting

setopt append_history         # append history list to the history file; this is the default but we make sure because it's required for share_history.
setopt auto_cd                # Shortcuts for directories e.g. hash -d
setopt auto_pushd             # make cd push the old directory onto the directory stack.
setopt complete_in_word       # not just at the end
setopt extended_glob          # in order to use # , ~ and ^ for filename generation grep word *~(*.gz|*.bz|*.bz2|*.zip|*.Z) -> searches for word not in compressed files don't forget to quote '^', '~' and ' # '!
setopt extended_history       # save each command's beginning timestamp and the duration to the history file
setopt globdots               # Glob Dotfiles As Well
setopt hash_list_all          # whenever a command completion is attempted, make sure the entire command path is hashed first.
setopt hist_expire_dups_first # Expire A Duplicate Event First When Trimming History.
setopt hist_find_no_dups      # Do Not Display A Previously Found Event.
setopt hist_ignore_all_dups   # If a new command line being added to the history list duplicates an older one, the older command is removed from the list
setopt hist_ignore_space      # remove command lines from the history list when the first character on the line is a space
setopt interactive_comments   # Allow comments even in interactive shells.
setopt long_list_jobs         # display PID when suspending processes as well
setopt noflowcontrol          # disable shell flowcontrol to reclaime C-s and C-q key bindings
setopt notify                 # report the status of backgrounds jobs immediately
setopt prompt_subst           # allow prompt substitution
setopt pushd_ignore_dups      # don't push the same dir twice.
setopt pushd_minus            # Exchanges the meanings of ‘+’ and ‘-’ when used with a number to specify a directory in the stack.
setopt pushd_silent           # Do not print the directory stack after pushd or popd.
setopt pushd_to_home          # Have pushd with no arguments act like ‘pushd $HOME’.
setopt share_history          # import new commands from the history file also in other zsh-session

# Autoload
autoload -Uz add-zsh-hook     # enable adding hooks to special functions
autoload -Uz colors && colors # enable named colors in zsh
autoload -Uz compinit         # enable completion system

# Parameters
COMPDUMPFILE="${XDG_CACHE_HOME}/zsh/compdump"
HISTFILE="${XDG_STATE_HOME}/zsh/history"                                                           # The file to save the history in when an interactive shell exits. If unset, the history is not saved.
HISTSIZE=1000000                                                                                   # The maximum number of events stored in the internal history list.
REPORTTIME=10                                                                                      # command runtime in seconds from which on the command will be reported
SAVEHIST=9000000                                                                                   # The maximum number of history events to save in the history file.
TIMEFMT="'$fg[green]%J$reset_color' time: $fg[blue]%*Es$reset_color, cpu: $fg[blue]%P$reset_color" # format for the output of the `time` prefix
WORDCHARS=''                                                                                       # A list of non-alphanumeric characters considered part of a word by the line editor.
ZSH_CACHE_DIR="${XDG_CACHE_HOME}/zsh"

if [[ -n ${COMPDUMPFILE}(#qN.mh+24) ]]; then
  compinit -d "${COMPDUMPFILE}"
else
  compinit -d "${COMPDUMPFILE}" -C
fi

bindkey -e

# Execute code in the background to not affect the current session
(
  setopt extended_glob local_options
  autoload -U zrecompile

  # Compile zcompdump, if modified, to increase startup speed.
  if [[ -s "${COMPDUMPFILE}" && (! -s "${COMPDUMPFILE}.zwc" || "${COMPDUMPFILE}" -nt "${COMPDUMPFILE}.zwc") ]]; then
    zrecompile -pq "${COMPDUMPFILE}"
  fi

  # zcompile .zshrc
  zrecompile -pq "${HOME}/.zshenv"
  zrecompile -pq "${ZDOTDIR}/.zshrc"
  zrecompile -pq "${ZDOTDIR}/.zprofile"

  # recompile all zsh or sh
  for f in ${ZDOTDIR}/**/*.*sh; do
    zrecompile -pq "${f}"
  done

  for p in ${XDG_CACHE_HOME}/antidote/**/*.*sh; do
    zrecompile -pq "${p}"
  done
) &!


[ -f "${ZDOTDIR}/variables.zsh" ] && source "${ZDOTDIR}/variables.zsh"


# Validate stow config status
dotfilesDir="${HOME}/dotfiles"

for repo in ${dotfilesDir}/*; do
  if [ -d "${repo}" ]; then
    git_update=$(git -C "${repo}" log -1 --pretty=format:%ct)
    stow_update=$(<"${repo}/.stow_update")

    [ ${git_update} -gt ${stow_update} ] && "${repo}/apply.sh" --simulate
  fi
done


# OS specific code
source "${ZDOTDIR}/os.zsh"


### --- plugins configuration - start ---
## powerlevel10k
typeset -g POWERLEVEL9K_DISABLE_HOT_RELOAD=true
typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet
typeset -g POWERLEVEL9K_MODE=nerdfont-complete

# command_execution_time
typeset -g POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND=0
typeset -g POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND=240
typeset -g POWERLEVEL9K_COMMAND_EXECUTION_TIME_PRECISION=1
typeset -g POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=0

# context
typeset -g DEFAULT_USER=${USER}

# dir
typeset -g POWERLEVEL9K_DIR_ANCHOR_BOLD=true
typeset -g POWERLEVEL9K_DIR_MAX_LENGTH=80
typeset -g POWERLEVEL9K_DIR_SHORTENED_FOREGROUND=249
typeset -g POWERLEVEL9K_DIR_TRUNCATE_BEFORE_MARKER=first
typeset -g POWERLEVEL9K_SHORTEN_DELIMITER=
typeset -g POWERLEVEL9K_SHORTEN_DIR_LENGTH=0
typeset -g POWERLEVEL9K_SHORTEN_STRATEGY=truncate_to_unique

# os_icon
typeset -g POWERLEVEL9K_OS_ICON_FOREGROUND=232
typeset -g POWERLEVEL9K_OS_ICON_BACKGROUND=7

# prompt style
typeset -g POWERLEVEL9K_MULTILINE_FIRST_PROMPT_GAP_CHAR='─'
typeset -g POWERLEVEL9K_MULTILINE_FIRST_PROMPT_GAP_FOREGROUND=244
typeset -g POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX='%244F╭─'
typeset -g POWERLEVEL9K_MULTILINE_NEWLINE_PROMPT_PREFIX='%244F├─'
typeset -g POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX='%244F╰─ '
typeset -g POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
typeset -g POWERLEVEL9K_PROMPT_CHAR_LEFT_RIGHT_WHITESPACE=
typeset -g POWERLEVEL9K_PROMPT_ON_NEWLINE=true

# status
typeset -g POWERLEVEL9K_STATUS_EXTENDED_STATES=true
typeset -g POWERLEVEL9K_STATUS_OK_PIPE=true

# used segments
typeset -g POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon ssh root_indicator context dir_writable dir direnv prompt_char newline vcs)
typeset -g POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status command_execution_time background_jobs)

# vcs
typeset -g POWERLEVEL9K_SHOW_CHANGESET=true
typeset -g POWERLEVEL9K_VCS_{STAGED,UNSTAGED,UNTRACKED,CONFLICTED,COMMITS_AHEAD,COMMITS_BEHIND}_MAX_NUM=-1

[ -f "${ZDOTDIR}/plugin_config.zsh" ] && source "${ZDOTDIR}/plugin_config.zsh"
### --- plugins configuration - end ---


### --- plugin manager configuration - begin ---
[ ! -f "${ZDOTDIR}/plugins.sh" ] && source "${ZDOTDIR}/update_plugins.zsh"

source "${ZDOTDIR}/plugins.sh"

# load the powerlevel10k prompt
autoload -Uz promptinit && promptinit
prompt powerlevel10k
### --- plugin manager configuration - end ---

zstyle ':completion:*:make:*:targets' call-command true
zstyle ':completion:*:*:make:*' tag-order 'targets'


### --- extension functions - start ---
# automatic directory listing and git status on cd command
function do-ls() {
  emulate -L zsh

  eza --icons --long --group --classify --time-style=long-iso --sort=.name --git --tree --level=1

  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == true ]]; then
    echo
    git status .
  fi
}
add-zsh-hook chpwd do-ls

# custom keybindings for string operations
function toggleSingleString() {
  LBUFFER=`echo $LBUFFER | sed "s/\(.*\) /\1 '/"`
  RBUFFER=`echo $RBUFFER | sed "s/\($\| \)/' /"`
  zle redisplay
}
zle -N toggleSingleString

function toggleDoubleString() {
  LBUFFER=`echo $LBUFFER | sed 's/\(.*\) /\1 "/'`
  RBUFFER=`echo $RBUFFER | sed 's/\($\| \)/" /'`
  zle redisplay
}
zle -N toggleDoubleString

function clearString() {
  LBUFFER=`echo $LBUFFER | sed 's/\(.*\)\('"'"'\|"\).*/\1\2/'`
  RBUFFER=`echo $RBUFFER | sed 's/.*\('"'"'\|"\)\(.*$\)/\1\2/'`
  zle redisplay
}
zle -N clearString

# get last modified file
function get-last-modified-file() {
  LAST_FILE=$(\ls -t1p | grep -v / | head -1)
  LBUFFER+=${(%):-$LAST_FILE}
}
zle -N get-last-modified-file

# jump behind the first word on the cmdline useful to add options.
function jump_after_first_word() {
  local words
  words=(${(z)BUFFER})

  if (( ${#words} <= 1 )) ; then
    CURSOR=${#BUFFER}
  else
    CURSOR=${#${words[1]}}+1
  fi
}
zle -N jump_after_first_word
### --- extension functions - end ---


# key binidngs
bindkey "\e[3~" delete-char
bindkey '^[[1;5C' forward-word  # [Ctrl-RightArrow] - move forward one word
bindkey '^[[1;5D' backward-word # [Ctrl-LeftArrow] - move backward one word
bindkey "^x'" toggleSingleString
bindkey '^x"' toggleDoubleString
bindkey '^x;' clearString
bindkey "^x1" jump_after_first_word
bindkey '^xl' get-last-modified-file


### --- aliases and command functions - begin ---
# disable old `git checkout` aliases
unalias gcb gcd gcm gco gcor

# disable old `git pull` alias
unalias gup

# global aliases for pipes and redirects
alias -g H='| head'
alias -g L="| less"
alias -g T='| tail'
alias -g LL="2>&1 | less"
alias -g NE="2> /dev/null"
alias -g NUL="> /dev/null 2>&1"

# command replacement aliases and functions
alias cp='cp --interactive'
alias df='dfrs --more'
alias diff='diff --color=auto'
alias dmesg='dmesg --reltime'
alias fd='fd --hidden'
alias ip='ip --color -brief'
alias ldd='lddtree --all'
alias mkdir='nocorrect mkdir --parents --verbose'
alias mv='mv --interactive --verbose'
alias ping='gping'
alias rm='rm --interactive=always --verbose'

function cat() {
  ext=$1:t:e
  if [ "${ext}" = "md" ]; then
    glow "${@}"
    return
  fi

  bat "${@}"
}

function cd () {
  # smart cd function, allows switching to /etc when running 'cd /etc/fstab'

  if (( ${#argv} == 1 )) && [[ -f ${1} ]]; then
    [[ ! -e ${1:h} ]] && return 1
    print "Correcting ${1} to ${1:h}"
    builtin cd ${1:h}
  else
    builtin cd "${@}"
  fi
}

function tree {
  eat 10 ${@}
}

# other aliases and functions
alias dh='cd $(dirs -pl | sk)'
alias docker_cleanup='docker system prune --volumes --all'
alias e='eza --icons --long --group --classify --time-style=long-iso --sort=.name --git --tree --level=1'
alias e.='eza --icons --long --group --classify --time-style=long-iso --sort=.name --git --list-dirs .*'
alias ea='eza --icons --long --group --classify --time-style=long-iso --sort=.name --git --all --tree --level=1'
alias formatkey="sed --regexp-extended ':a;N;\$!ba;s/\\r{0,1}\\n/\\\\n/g'"
alias git_branch_cleanup='git branch --merged | grep -v $(git_main_branch) | xargs git branch -d; git branch --no-merged | grep -v $(git_main_branch) | xargs git branch -D'
alias hadolint='docker run -v $(pwd):/project:ro --workdir=/project --rm -i hadolint/hadolint hadolint'
alias lg='lazygit'
alias nocomment="grep '^[[:blank:]]*[^[:blank:]#;]'"
alias segaja.de='ssh segaja.de -t LC_ALL=en_US.UTF-8 zellij attach segaja.de'
alias t='tail --follow'
alias usbdevice='sudo usbctl temporary'

function et() {
  eza --icons --long --group --classify --time-style=long-iso --sort=.name --git --tree -L ${@}
}
function eat() {
  eza --all --icons --long --group --classify --time-style=long-iso --sort=.name --git --tree -L ${@}
}

function formattimestamp() {
  date -d @${1}
}

function mkdircd() {
  mkdir --parents --verbose ${1}
  cd ${1}
}

function p {
  [ -n "${ZELLIJ}" ] && zellij action rename-tab "${1}"

  pj "${1}"
}

function zed() {
  zellij edit --direction=down "$*"
}
function zef() {
  zellij edit --floating "$*"
}
function zer() {
  zellij edit --direction=right "$*"
}
function zr() {
  zellij run --name "$*" --close-on-exit -- zsh -ic "$*"
}
function zrf() {
  zellij run --name "$*" --close-on-exit --floating -- zsh -ic "$*"
}


[ -f "${ZDOTDIR}/aliases.zsh" ] && source "${ZDOTDIR}/aliases.zsh" ||:
### --- aliases and command functions - end---

#!/usr/bin/env bash

paru -Syu --needed \
    arandr \
    bbswitch-dkms \
    blueman \
    bluez \
    bluez-libs \
    bluez-utils-compat \
    filezilla \
    fzf \
    nvidia-dkms \
    nvidia-settings \
    primus \
    pulseaudio-bluetooth \
    wpa_supplicant

#!/usr/bin/env bash

headline_count=$(rg --count --regexp '(^|\s+)printHeadline "' .local/bin/maintenance)
count=0
desktop="0"

if [ -n "${1}" ] && [ "--desktop" = "${1}" ]; then
  desktop="1"
fi


printHeadline() {
  ((count++))
  headline="(${count} / ${headline_count}): ${1} ..."

  echo -e "\e[1;34m>>> \e[0;1m${headline}\e[0m"
}


printHeadline "Install needed packages"

pacman -Syu --needed \
  base-devel \
  git \
  inetutils \
  laptop-detect \
  openssh
echo


printHeadline "Install paru"

tempDir="$(mktemp --directory)"

cd "${tempDir}"
git clone https://aur.archlinux.org/paru.git
pushd "./paru"

makepkg --clean --install --rmdeps --syncdeps
popd

rm --recursive "${tempDir}"
echo


if [ "1" = "${desktop}" ]; then
  printHeadline "Install desktop packages"

  paru -Syu --needed \
    xorg-server \
    xorg-xinit
  echo
else
  ((headline_count--))
fi


printHeadline "Run install packages script"
./install-packages.sh

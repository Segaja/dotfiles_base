#!/usr/bin/env bash

targets_unsorted=(
  "base"
  "distro_$(awk -F '=' '/^ID=/{print $2}' /etc/os-release)"
  "host_$(hostname)"
)

laptop-detect && {
  targets_unsorted+=("laptop")
}

[[ $EUID -ne 0 ]] && hash X 2> /dev/null && {
  targets_unsorted+=("desktop")
}

[[ -z "${SSH_CONNECTION}" ]] && {
  targets_unsorted+=("local")
}

readarray -td '' targets < <(printf '%s\0' "${targets_unsorted[@]}" | sort -z)


apply_targets=()
install_targets=()
repo_path="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"

for target in "${targets[@]:?}"; do
  if [ -f "${repo_path}/${target}/_install-packages.sh" ]; then
    install_targets+=("${target}")
  fi

  if [ -d "${repo_path}/${target}" ]; then
    apply_targets+=("${target}")
  else
    echo "No target folder for target '${target}' found"
  fi
done

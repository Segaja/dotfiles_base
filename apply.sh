#!/usr/bin/env bash

repo_path="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"

migrations() {
  if [ -d "${XDG_CONFIG_HOME}/vim/pack/base/start/tabular" ]; then
    rm -rf "${XDG_CONFIG_HOME}/vim/pack/base/start/tabular"
  fi
}

source "${repo_path}/_apply.sh"

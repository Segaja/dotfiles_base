POWERLEVEL9K_DISK_USAGE_BACKGROUND=0
POWERLEVEL9K_DISK_USAGE_FOREGROUND=240
POWERLEVEL9K_LOAD_NORMAL_BACKGROUND=0
POWERLEVEL9K_LOAD_NORMAL_FOREGROUND=240
POWERLEVEL9K_RAM_BACKGROUND=0
POWERLEVEL9K_RAM_FOREGROUND=240
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status command_execution_time background_jobs disk_usage ram load time)
POWERLEVEL9K_TIME_BACKGROUND=0
POWERLEVEL9K_TIME_FOREGROUND=240

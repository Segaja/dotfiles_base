" set a nice font, pleasant for my eyes:
if has('GUI_GTK2')
  set guifont=xos4\ Terminus\ 13
elseif has('xfontset')
  set guifont=xos4-Terminus-13
endif
 
set guicursor=a:blinkon0 " stop cursor from blinking:
set guioptions-=m    " remove menu bar
set guioptions-=T    " remove toolbar
set guioptions-=r    " remove right hand scroll bar
set guioptions-=L    " remove left hand scroll bar
set nomousehide      " do not hide mouse

#!/usr/bin/env bash

DEBUG=0

if [ ${#} -lt 1 ]; then
    echo "Usage: ${0} <status|toggle>"
    exit 1
fi

getActiveDevice() {
    # $1 type (source, sink)

    pacmd info | grep "Default ${1} name" | cut -d':' -f2 | cut -d'.' -f2 | cut -d'-' -f1
}

case "${1}" in
    "status")
        getActiveDevice "sink"

        ;;

    "sink-headphones")
        target_sink="alsa_output.usb-KTMicro_KT_USB_Audio_2021-06-07-0000-0000-0000--00.analog-stereo"

        pactl set-default-sink "${target_sink}"

        ;;

    "sink-jabra")
        target_sink="alsa_output.usb-0b0e_Jabra_SPEAK_510_USB_08C8C2AA1EE8022000-00.analog-stereo"

        pactl set-default-sink "${target_sink}"

        ;;

    "sink-speakers")
        target_sink="alsa_output.pci-0000_0c_00.4.analog-stereo"

        pactl set-default-sink "${target_sink}"

        ;;

    "sink-toggle")
        target="usb"

        sink_status="$(getActiveDevice "sink")"

        if [ "${sink_status}" = "usb" ]; then
            target="pci"
        fi

        target_sink="$(pactl list short sinks | grep "analog-stereo" | grep "${target}" | tr '\t' ' ' | cut -d' ' -f2)"

        if [ ${DEBUG} -eq 1 ]; then
            echo "target: ${target}"
            echo "target sink: ${target_sink}"
        fi

        pactl set-default-sink "${target_sink}"

        ;;

    "source-headphones")
        target_source="alsa_input.usb-KTMicro_KT_USB_Audio_2021-06-07-0000-0000-0000--00.mono-fallback"

        pactl set-default-source "${target_source}"

        ;;

    "source-jabra")
        target_source="alsa_input.usb-0b0e_Jabra_SPEAK_510_USB_08C8C2AA1EE8022000-00.mono-fallback"

        pactl set-default-source "${target_source}"

        ;;

    *)
        >&2 echo "Unregonized option '${1}'. Valid options are: status, sink-headphones, sink-jabra, sink-speakers, sink-toggle, source-headphones, source-jabra"
        exit 1
esac

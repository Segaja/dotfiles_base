# maintenance
alias maintenance='zellij --session maintenance --new-session-with-layout maintenance'

# command not found
source /usr/share/doc/pkgfile/command-not-found.zsh

# skim
source /usr/share/skim/key-bindings.zsh


# archlinux build system
export PATH="${HOME}/develop/gitlab.com/segaja/arch-repro-tools:${PATH}"

alias arch-clear-package-tmp='rm -rf ${XDG_CACHE_HOME}/archlinux/{logs,packages,sources}/*'


nv_configfolder="${HOME}/.config/nvchecker/sources"

function nvc() {
  local config repo

  if ! command -v nvchecker > /dev/null; then
    1>&2 printf "The required application 'nvchecker' can not be found.\n"
    return 1
  fi

  if [[ -z "${1}" ]]; then
    printf "No repository name specified as the first argument => running nvchecker for all config files under ${nv_configfolder}.\n\n"

    for config in ${nv_configfolder}/*.toml; do
      repo=$config:t:r
      echo "\e[1;34m>>> \e[0;1m[${repo}]\e[0m"
      nvchecker -c "${config}"
      echo
    done

    return
  fi

  config="${nv_configfolder}/${1}.toml"

  if [[ ! -f "${config}" ]]; then
    1>&2 printf "The configuration does not exist: %s\n" "${config}"
    return 1
  fi

  shift

  nvchecker -c "${config}" ${@}
}

function nvt() {
  local config package

  if ! command -v nvtake > /dev/null; then
    1>&2 printf "The required application 'nvtake' can not be found.\n"
    return 1
  fi

  if [[ -z "$1" ]]; then
    1>&2 printf "A repository name needs to be specified as the first argument.\n"
    return 1
  fi

  config="${nv_configfolder}/${1}.toml"

  if [[ ! -f "${config}" ]]; then
    1>&2 printf "The configuration does not exist: %s\n" "${config}"
    return 1
  fi

  if [[ -z "${2}" ]]; then
    1>&2 printf "A package name needs to be specified as the second argument.\n"
    return 1
  fi

  package="${2}"
  package_name="${2%=*}"

  if ! grep "${package_name}" "${config}" > /dev/null; then
    1>&2 printf "The package %s can not be found in the configuration: %s\n" "${package_name}" "${config}"
    return 1
  fi

  nvtake -c "${config}" "${package}"
}

function _nvc() {
  _arguments \
    "1:Arch Linux repository:( ${nv_configfolder}/*.toml(:t:r) )"
}

compdef _nvc nvc

function _nvt() {
  _arguments \
    "1:Arch Linux repository:( ${nv_configfolder}/*.toml(:t:r) )" \
    '2:Package:( "${(f)$(awk "match(\$0, /^\[(.*)\]/, a) {print a[1]}" ${nv_configfolder}/$line[1].toml | grep -v "__config__")}" )'
}

compdef _nvt nvt


arch_repo_folder="${HOME}/develop/gitlab.archlinux.org/archlinux/packaging/packages"

function arch-mkpkg() {
  local pkgfolder="${arch_repo_folder}/${1}"

  mkdir --parents --verbose "${pkgfolder}/"

  touch "${pkgfolder}/PKGBUILD"

  pkgctl repo create --clone "${1}"

  cd "${pkgfolder}"
}

function arch-pkg-cd() {
  local pkgfolder="${arch_repo_folder}/${1}"

  if [ ! -d "${pkgfolder}" ]; then
    echo "package folder for ${1} doesn't exist localy. checking remotely..."

    if pacman -Si "${1}" > /dev/null; then
      cd "${arch_repo_folder}" > /dev/null

      pkgctl repo clone "${1}"
    elif [ "${2}" = "-c" ]; then
      mkdir --parents --verbose "${pkgfolder}"
    else
      return
    fi
  fi

  [ -n "${ZELLIJ}" ] && zellij action rename-tab "${1}"

  cd "${pkgfolder}"

  echo
  pkgctl repo configure
  echo
  git pull --rebase
  echo
  git status
}

function _arch-pkg-cd() {
  _arguments \
    "1:Arch Linux packages:( ${arch_repo_folder}/*(:t) )"
}

compdef _arch-pkg-cd arch-pkg-cd

#!/usr/bin/env bash

params="${*:--R}"

source "${repo_path:?}/_detect-targets.sh"

if [ "${params}" = "-R" ]; then
  date +%s > "${repo_path}/.stow_update"
fi


mkdir --parents --verbose \
  ~/.cache/archlinux/{logs,packages,sources} \
  ~/.cache/zsh/completions \
  ~/.local/{bin,state} \
  ~/.local/state/zsh

# shellcheck disable=SC2068
(set -x; stow --dir="${repo_path}" --target="${HOME}" --no-folding --verbose=1 "${params}" ${apply_targets[@]})

if [ "--simulate" = "${params}" ]; then
  echo -e "\nno migration handling in simulation mode"
  exit 0
fi

if  [ "$(type -t migrations)" = function ]; then
  echo -e "\n >> runnig migrations"
  migrations
fi

if [ "$(id -u)" -eq 1000 ]; then
  echo " >> reloading systemd user services"
  systemctl --user daemon-reload
fi

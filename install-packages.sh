#!/usr/bin/env bash

source _detect-targets.sh

for target in "${install_targets[@]:?}"; do
  echo -e "\033[1;34m::::\033[0;1m Installing \033[0m${target}\033[1m packages...\033[0m"
  echo "${repo_path}/${target}/_install-packages.sh"
  echo
done

#!/usr/bin/env bash

paru -Syu --needed \
    amd-ucode \
    borg \
    davfs2 \
    direnv \
    nodejs-hueadm \
    phpstorm \
    tfenv \
    time \
    xf86-video-amdgpu

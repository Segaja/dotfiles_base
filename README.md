# Segaja's dotfiles

This repository holds all of my config files for my machine setup.

## Targets
| target     | when to apply?                                                         | description                                                                                                             |
|------------|------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| `base`     | always                                                                 | all the basic console and system configuration (e.g.: zellij, vim, zsh, ...)                                              |
| `distro_*` | based on which OS is detected                                          | file specific to the OS that is used (currently only arch is supported) like different paths for certain configurations |
| `local`    | when you are not connected via SSH to the machine you run the apply on | some local configuration (like ssh) which doesn't make much sense on remote machines                                    |
| `desktop`  | only when the `$DISPLAY` variable is set                               | configuration for my archlinux i3 desktop setup                                                                         |
| `host_*`   | if the hostname of the machine matches the target name                 | configuration specific to certain machines/servers I use                                                                |

## How to install

### Prerequisites
The structure of this  repository with the targets above is based on [GNU Stow](https://www.gnu.org/software/stow/manual/stow.html).

### Install required packages
There is the `install-packages.sh` script which assumes that you use [Arch Linux](https://archlinux.org/) and from that the [AUR](https://aur.archlinux.org/) package manager helper [Paru](https://aur.archlinux.org/packages/paru) in order to handle upstream packages and AUR packages very similar.
If you use a different operating system you need to find out where to get all the packages from.

Each of the above targets can define the packages which are needed to run the systems configured by this target. These files can be found in the target folders as `_install-packages.sh` and will automatically be included if the target would be applied.

### Apply targets
To apply the targets you can use the [`apply.sh`](./apply.sh) script which automatically does all the target detection and re-applies all targets. It also writes the date of the last apply run to the `.stow_update` file in the root folder of the repository.
